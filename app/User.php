<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
  protected $table = 'users';
  public $timestamps = true;

  protected $fillable = [
     'name', 'email', 'phone', 'birth'
  ];

  public function addresses()
  {
      return $this->hasMany('App\Address');
  }
}

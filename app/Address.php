<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';
    public $timestamps = true;

    protected $fillable = [
        'user_id', 'zip_code', 'state', 'country', 'city', 'district', 'street', 'number', 'complement'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}

<?php

namespace App\Http\Controllers;
use App\User;
use App\Address;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /*
    * [GET] Função que retorna a lista de usuários cadastrados
    */
    public function getAllUsers()
    {
      $users = User::all();
      return $users;
    }

    /*
    * [GET] Função que retorna a paǵina de index do sistema
    */
    public function index()
    {
      return view('welcome');
    }

    /*
    * [POST] Função para salvar as informações do usuário
    */
    public function postInfo(Request $request)
    {
      $data = $request->all();

      $user = new User();
      $user->name = $data['name'];
      $user->email = $data['email'];
      $user->phone = $data['tel'];
      $user->birth = $data['birth'];

      if($user->save())
      {
        $address = new Address();
        $address->user_id = $user->id;
        $address->country = "Brasil";
        $address->zip_code = $data['zip_code'];
        $address->street = $data['street'];
        $address->state = $data['state'];
        $address->city = $data['city'];
        $address->district = $data['district'];
        $address->complement = $data['complement'];
        $address->number = ($data['number'] != '' ? $data['number'] : 0);
        $address->save();
      }

      return $user;
    }
}

$(document).ready(function(){

  $('#tel').mask('(00) 0000-00009');

  $("#zip_code").mask("99.999-999");

  $('#contactForm').submit(function () {
    $('.container-box').hide();
    $('.container-saving').show();

    let url = 'http://localhost:8000/user';
    let dataJson = {
      name: $("#name").val(),
      email: $("#email").val(),
      tel: $("#tel").val(),
      birth: $("#birth").val(),
      zip_code: $("#zip_code").val(),
      street: $("#street").val(),
      state: $("#state").val(),
      city: $("#city").val(),
      district: $("#district").val(),
      number: $("#number").val(),
      complement: $("#complement").val()
    };

    var posting = $.post(url, dataJson)
      .done(function(data) {
        console.log(data);

        //Atraso de 3 segundos para mostrar o texto de 'salvando suas informações', já que a operação de salvar é muito rápida.
        setTimeout(function(){
          $('.container-saving img').attr("src","/img/saved.png");
          $('.container-saving span').text("Informações salvas com sucesso.");
        },3000);

      })
      .fail(function(xhr, status, error) {
        console.log(error);

        //Atraso de 3 segundos para mostrar o texto de 'salvando suas informações', já que a operação de salvar é muito rápida.
        setTimeout(function(){
          $('.container-saving img').attr("src","/img/fail.png");;
          $('.container-saving span').text("Erro ao salvar informações.");
        },3000);
      });

    return false;
  });
});

function changeCep(inputCep)
{
  let cep = inputCep.value;
  console.log(cep.length);

  //Verifica se o cep é válido para consulta
  if(cep.length == 10)
  {
    $('.container-box img').show();
    cep = cep.replace(/[^0-9]/g,'');
    $.getJSON( "https://webmaniabr.com/api/1/cep/"+cep+"/?app_key=wmIW5RWYZxBqZbiRiZPjBddOHthnJIiZ&app_secret=dKcpS6CqCGAcXn8yawWTi96ntS7GZbTLwsvhIV0ABBbFHMx6", function( data ) {
      console.log(data);
      $('.container-box img').hide();
      if(data.endereco != '')
      {
        $("#street").val(data.endereco);
        $("#state").val(data.uf);
        $("#city").val(data.cidade);
        $("#district").val(data.bairro);
      } else {
        $("#street").val('Não encontrado');
        $("#state").val('Não encontrado');
        $("#city").val('Não encontrado');
        $("#district").val('Não encontrado');
        $("#zip_code").val('');
      }
    });
  } else {
    inputCep.value = '';
  }
}

function changeTel(inputTel)
{
  let tel = inputTel.value;
  if(tel.length < 14)
  {
    inputTel.value = '';
  }
}

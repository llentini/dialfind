## Sobre
Sistema implementado utilizando a ultima versão do Laravel com o intuito de cadastrar informações de usuários através de uma Landing Page.

## Back-end
Laravel 5.5 + MySQL

## Front-end
Bootstrap + JQuery

## Consulta do CEP
API do Correios

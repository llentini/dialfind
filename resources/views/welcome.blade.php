<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>DialInfo</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Bootstrap css -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

        <link rel="stylesheet" href="/css/app.css">
    </head>

    <body>

      <div class="container container-box my-5 justify-content-center">
      	<div class="row justify-content-center">

            <div class="col-lg-6 right-box">
              <form id="contactForm">
                <div class="row">

                  <span> Dados Pessoais </span>

                  <div class="col-lg-12">
                    <div class="form-group">
                      <label for="nome">Nome *</label>
                      <input type="text" class="form-control" id="name" placeholder="Digite seu nome" required>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label for="email">Email *</label>
                      <input type="email" class="form-control" id="email" placeholder="Digite seu email" required>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label>Telefone *</label>
                      <input type="tel" class="form-control" id="tel" placeholder="(00) 0000-00000" onblur="changeTel(this)" required>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label>Data de Nascimento *</label>
                      <input type="date" class="form-control" id="birth" placeholder="" required>
                    </div>
                  </div>


                  <span> Endereço </span>

                  <div class="col-lg-12">
                    <div class="form-group">
                      <label>CEP *</label>
                      <input type="text" class="form-control" id="zip_code" placeholder="00.000-000" onblur="changeCep(this)" required>
                    </div>
                  </div>

                  <div class="col-lg-12">
                    <div class="form-group">
                      <label>Rua </label>
                      <img src="/img/loading.gif"> <input type="text" class="form-control" id="street" placeholder=""  readonly>
                    </div>
                  </div>


                 <div class="col-lg-6">
                    <div class="form-group">
                      <label>Estado </label>
                        <img src="/img/loading.gif"> <input type="text" class="form-control" id="state" placeholder=""  readonly>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label>Cidade </label>
                      <img src="/img/loading.gif"> <input type="text" class="form-control" id="city" placeholder=""  readonly>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label>Bairro  </label>
                      <img src="/img/loading.gif"> <input type="text" class="form-control" id="district" placeholder=""  readonly>
                    </div>
                  </div>

                  <div class="col-lg-3">
                    <div class="form-group">
                      <label>Número </label>
                      <input type="text" class="form-control" id="number" placeholder="" >
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label>Complemento</label>
                      <input type="text" class="form-control" id="complement" placeholder="" >
                    </div>
                  </div>

                </div>

                <button type="submit" class="btn btn-primary float-right">Salvar</button>
              </form>
          	</div>

      	</div>
      </div>


      <!-- Container visivel quando o usuário clicar em salvar -->
      <div class="container container-saving my-5 justify-content-center">
          <div class="row justify-content-center">
              <div class="col-lg-6 save-box">
                  <img src="/img/loading.gif"> <span> Salvando suas informações, aguarde </span>
              </div>
          </div>
      </div>
    </body>

    <!-- Bootstrap js -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

    <!-- JQuery js -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

    <!-- Script da página -->
    <script src="/js/myscript.js"></script>
</html>
